using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class Audio : MonoBehaviour
{

    #region Singleton

    private static Audio _instance;

    public static Audio Instance => _instance ??= FindObjectOfType<Audio>();

    private void Awake()
    {
        if (_instance && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        Init();
    }

    #endregion

    public static AudioLibrary Library => Instance._library;

    private AudioSource _sfx, _music1, _music2;
    private AudioLibrary _library;
    private const float Volume = .5f;

    private void Init()
    {
        (_sfx = new GameObject("SFX").AddComponent<AudioSource>()).transform.parent = transform;
        (_music1 = new GameObject("M1").AddComponent<AudioSource>()).transform.parent = transform;
        (_music2 = new GameObject("M2").AddComponent<AudioSource>()).transform.parent = transform;
        _sfx.volume = _music1.volume = _music2.volume = Volume;
        _music1.loop = _music2.loop = true;
        _library = Resources.Load<AudioLibrary>("library");
    }

    private void Start()
    {
        _music1.clip = _library.menu;
        _music2.clip = _library.game;
        _music1.Play();
    }

    public void FadeToGameMusic() => Fade(_music1, _music2);
    public void FadeToMenuMusic() => Fade(_music1, _music2);


    private async void Fade(AudioSource a, AudioSource b)
    {
        b.volume = 0f;
        b.Play();
        var currentTime = 0f;
        var start = 0f;
        var end = Volume;
        
        while (currentTime < 1f)
        {
            currentTime += Time.fixedDeltaTime;
            a.volume = Mathf.Lerp(end, start, currentTime);
            b.volume = Mathf.Lerp(start, end, currentTime);
            await UniTask.WaitForFixedUpdate();
        }
        
        a.Stop();
    }

    public void Play(AudioClip clip) => _sfx.PlayOneShot(clip, Volume*1.5f);
}
