﻿using UnityEngine;

[CreateAssetMenu(fileName = "library", menuName = "Game/Lib", order = 0)]
public class AudioLibrary : ScriptableObject
{
    public AudioClip[] claps;

    public AudioClip gunShoot,
        gunFailedShoot,
        gunReload,
        lightsOff,
        ghostSpawn,
        ghostTapped,
        ghostVictory,
        goblinSpawn,
        goblinTapped,
        goblinVictory;

    [Space] public AudioClip menu, game;


}
