﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

[Serializable]
public class Pool<T> where T : Component
{
	private readonly T prefab;

	[HideInInspector]
	public Func<Component, bool> canBeUsedCondition = x => { return !x.gameObject.activeInHierarchy; };

	[HideInInspector] public Action<T> OnCreated;

	public Pool(T prefab)
	{
		this.prefab = prefab;
	}

	public List<T> pooledObjects { get; protected set; } = new List<T>();

	public T Get(Transform parent = null)
	{
		foreach (var o in pooledObjects)
			if (canBeUsedCondition(o))
			{
				o.transform.parent = parent;
				return o;
			}

		var go = CreateNew();
		go.transform.parent = parent;
		return go;
	}

	private T CreateNew()
	{
		var newObject = Object.Instantiate(prefab);
		pooledObjects.Add(newObject);
		OnCreated?.Invoke(newObject);
		return newObject;
	}
}