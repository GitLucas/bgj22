using UnityEngine;
using UnityEngine.UI;

public class Clapometer : MonoBehaviour
{
    [SerializeField] private Slider slider;
    private int _combo;
    private GameData _data;

    public int Value
    {
        get
        {
            return slider.value switch
            {
                > .75f => 3,
                > .5f => 2,
                > .25f => 1,
                _ => 0
            };
        }
    }

    public float Score => slider.value * 10;

    private void Start()
    {
        _data = Resources.Load<GameData>("data");
        ReachToTarget.LossCondition += HandleLossCondition;
        ReachToTarget.WinCondition += HandleWinCondition;
    }

    private void HandleWinCondition()
    {
        ++_combo;
        slider.value += _combo * _data.clapValue;
    }

    private void HandleLossCondition()
    {
        _combo = 0;
        slider.value -= _data.clapValue * 15;
    }

    private void OnDestroy()
    {
        ReachToTarget.LossCondition -= HandleLossCondition;
        ReachToTarget.WinCondition -= HandleWinCondition;
    }
}
