using Unity.VisualScripting;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{

    public float xMax, yMax;
    private Vector2 _middle;
    private float z;

    public bool MoveWithMouse { get; set; } = true;

    private void Start()
    {
        _middle = new Vector2(Screen.width/2f, Screen.height/2f);
        z = transform.position.z;
    }

    private void Update()
    {
        if (!MoveWithMouse) return;
        var mouse = Input.mousePosition;
        if (OutOfBounds(mouse)) return;
        var pos = ((Vector2)mouse) - _middle;
        var x = (pos.x / _middle.x) * xMax;
        var y = (pos.y / _middle.y) * yMax;
        transform.position = new Vector3(x, y, z);
    }

    private bool OutOfBounds(Vector2 pos)
    {
        return pos.x < 0 || pos.x > Screen.width ||
               pos.y < 0 || pos.y > Screen.height;
    }

}
