using System;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class Overlay : MonoBehaviour
{
    [SerializeField] private GameObject image;
    [SerializeField] private CanvasGroup group;
    
    private void Awake()
    {
        FadeIn();
    }

    private async void FadeIn()
    {
        while (group.alpha > 0)
        {
            group.alpha -= .1f;
            await UniTask.WaitForFixedUpdate();
        }

        group.alpha = 0;
        image.SetActive(false);
    }
    
    
    public async Task FadeOut()
    {
        image.SetActive(true);
        while (group.alpha < 1)
        {
            group.alpha += .1f;
            await UniTask.WaitForFixedUpdate();
        }

        group.alpha = 1;
    }
}
