using UnityEngine;

public class Curtain : MonoBehaviour
{
    [SerializeField] private GameManager game;
    [SerializeField] private GameObject[] disableOnStart;

    public void StartGame()
    {
        game.StartGame();
        foreach (var go in disableOnStart)
            go.SetActive(false);
    }
}
