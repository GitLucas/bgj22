using UnityEngine;

public class DisablerOnTrigger : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("disable")) col.gameObject.SetActive(false);
    }
}