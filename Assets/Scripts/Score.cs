﻿using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Overlay overlay;

    public void ShowScore(float score)
    {
        scoreText.text = $"{score} / 10";
    }

    public async void RestartGame()
    {
        await overlay.FadeOut();
        await UniTask.Delay(1000);
        SceneManager.LoadScene(0);
    }
        
}