using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject[] enableOnStart;
    [SerializeField] private GameObject[] disableOnEnd;
    [Space]
    [SerializeField] private Score score;
    [SerializeField] private Clapometer clapometer;
    [SerializeField] private InputManager input;
    [SerializeField] private Animator curtain;

    private GameData _data;

    private float _st;
    private bool _running;

    private void Start()
    {
        _data = Resources.Load<GameData>("data");
    }

    public void StartGame()
    {
        foreach (var go in enableOnStart)
            go.SetActive(true);
        _st = Time.time;
        _running = true;
    }

    private void Update()
    {
        if (!_running) return;
        if (Time.time - _st < _data.gameDuration) return;

        _running = false;
        EndGame();
    }

    private void EndGame()
    {
        score.ShowScore(clapometer.Score);
        curtain.SetTrigger("Close");
        Audio.Instance.Play(Audio.Library.claps[clapometer.Value]);
        Audio.Instance.FadeToMenuMusic();
        
        foreach (var go in disableOnEnd)
            go.SetActive(false);
    }
}
