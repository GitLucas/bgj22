﻿using Cysharp.Threading.Tasks;
using UnityEngine;

public abstract class EnemyTarget : MonoBehaviour
{
    [SerializeField] protected SpriteRenderer Renderer;
    [Space] 
    [SerializeField] private Sprite[] sprites; 
    
    public Vector3 Position => transform.position;
    private bool _interacting;

    public async void Interact()
    {
        if (_interacting) return;
        
        _interacting = true;
        
        PreInteraction();
        await UniTask.Delay(2000);
        PostInteraction();
        
        _interacting = false;
    }

    protected virtual void PreInteraction() => Renderer.sprite = sprites[1];
    protected virtual void PostInteraction() => Renderer.sprite = sprites[0];
    
}