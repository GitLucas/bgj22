using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform[] spawnPoints;
    [SerializeField] private EnemyTarget[] targetPoints;
    [SerializeField] private Interactable prefab;

    [SerializeField] private bool shiny;
    [SerializeField] private Interactable shinyPrefab;

    private GameData _data;
    
    private Pool<Interactable> _pool, _shinyPool;
    private float _delta = 5;
    
    private void Start()
    {
        _data = Resources.Load<GameData>("data");
        _pool = new Pool<Interactable>(prefab);
        if (shiny) _shinyPool = new Pool<Interactable>(shinyPrefab);
    }

    private void FixedUpdate()
    {
        _delta += Time.fixedDeltaTime;
        if (_delta <= _data.spawnDelay) return;
        var i = Random.Range(0, spawnPoints.Length);
        Spawn(GetEnemy(), i);
        Spawn(GetEnemy(), (i+1) % spawnPoints.Length);
        _delta = 0f;
    }

    private ReachToTarget GetEnemy()
    {
        if(shiny)
            if(Random.Range(0f, 1f) < 1f/256f)
                return _shinyPool.Get(transform) as ReachToTarget;
        return _pool.Get(transform) as ReachToTarget;
    }

    private void Spawn(ReachToTarget enemy, int i)
    {
        enemy.transform.position = spawnPoints[i].position;
        enemy.Target = targetPoints[Random.Range(0, targetPoints.Length)];
        enemy.Enable();
    }
}
