﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class PlayerView : MonoBehaviour
{

    #region Singleton

    private static PlayerView _instance;

    public static PlayerView Instance => _instance ??= FindObjectOfType<PlayerView>();

    private void Awake()
    {
        if (_instance && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
    }

    #endregion

    
    [SerializeField] public new MoveCamera camera;
    [SerializeField] public Animator animator;
    [SerializeField] public Gun gun;

    private UniTask _task = UniTask.CompletedTask;
    private CancellationTokenSource _hidects, _unhidects;

    private void Start()
    {
        _hidects = new CancellationTokenSource();
        _unhidects = new CancellationTokenSource();
        Hide();
    }

    public void Hide()
    {
        animator.SetBool("Hidden", true);
        gun.Reload();
    }
    
    public async void UnHide()
    {
        animator.SetBool("Hidden", false);
        gun.StopReload();
    }

    public void EnableMoveCamera() =>camera.MoveWithMouse = true; 
    public void DisableMoveCamera() =>camera.MoveWithMouse = false; 

}