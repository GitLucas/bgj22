using UnityEngine;

public class InputManager : MonoBehaviour
{

    #region Singleton

    private static InputManager _instance;

    public static InputManager Instance => _instance ??= FindObjectOfType<InputManager>();

    private void Awake()
    {
        if (_instance && _instance != this)
        {
            Destroy(gameObject);
            return;
        }

        _instance = this;
        Init();
    }

    #endregion

    [SerializeField] private PlayerView player;
    
    private void Init()
    {
    }

    public bool InputEnabled { get; set; }

    private bool UncoverPress => Input.GetMouseButtonDown(1) || Input.GetKeyDown(KeyCode.Space);
    private bool UncoverRelease => Input.GetMouseButtonUp(1) || Input.GetKeyUp(KeyCode.Space);

    private void Update()
    {
        if (UncoverPress)
            EnableShooting();

        if (UncoverRelease)
            DisableShooting();

        if(InputEnabled && Input.GetMouseButtonDown(0))
            player.gun.Shoot();
    }

    private void EnableShooting()
    {
        InputEnabled = true;
        player.UnHide();
    }

    private void DisableShooting()
    {
        InputEnabled = false;
        player.Hide();
    }
}
