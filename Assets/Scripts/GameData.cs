﻿using UnityEngine;

[CreateAssetMenu(fileName = "data", menuName = "Game/Data", order = 0)]
public class GameData : ScriptableObject
{
    public float npcSpeed;
    public float gameDuration;
    public float chargePerShot;
    public float spawnDelay;
    public float clapValue;
    public float floorPosition;

}