using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public abstract class Interactable : MonoBehaviour
{
    [SerializeField] protected SpriteRenderer Renderer;

    protected Collider2D Collider;
    protected SpriteRenderer Highlight;
    private bool _hovered;

    private void Awake()
    {
        Highlight = (new GameObject("highlight")).AddComponent<SpriteRenderer>();
        var ht = Highlight.transform;
        ht.parent = transform;
        Highlight.sprite = Renderer.sprite;
        Highlight.color = new Color(1 ,1, 0);
        ht.localScale = Vector3.one * 1.15f;
        ht.localPosition = Vector3.forward * 0.1f;
        Highlight.sortingOrder = Renderer.sortingOrder - 1;
        Highlight.sortingLayerName = Renderer.sortingLayerName;
        Highlight.enabled = false;
        
        Gun.OnShoot += GunOnOnShoot;

        Collider = GetComponent<Collider2D>();
    }

    private void GunOnOnShoot()
    {
        if (!_hovered) return;
        Execute();
    }

    private void OnMouseEnter() => Hover();
    private void OnMouseExit() => Unhover();

    public void Hover()
    {
        Highlight.enabled = _hovered = true;
    }

    public void Unhover()
    {
        Highlight.enabled = _hovered = false;
    }

    private void Reset()
    {
        Renderer = GetComponent<SpriteRenderer>();
    }

    public abstract void Execute();

    public virtual void Enable() => gameObject.SetActive(true);

    public void Disable()
    {
        Unhover();
        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Gun.OnShoot -= GunOnOnShoot;
    }
}
