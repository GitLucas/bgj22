using UnityEngine;

public class LevelReplay : Interactable
{
    [SerializeField] private Score parent;
    
    public override void Execute()
    {
        parent.RestartGame();
    }
}
