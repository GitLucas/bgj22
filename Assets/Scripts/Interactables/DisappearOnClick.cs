﻿public class DisappearOnClick : Interactable
{
    public override void Execute()
    {
        gameObject.SetActive(false);
    }
}