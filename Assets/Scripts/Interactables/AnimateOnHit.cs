using UnityEngine;

public class AnimateOnHit : EnemyTarget
{
    [SerializeField] private Animator anim;

    protected override void PreInteraction()
    {
        anim.SetTrigger("Hit");
    }

    protected override void PostInteraction() { }
}
