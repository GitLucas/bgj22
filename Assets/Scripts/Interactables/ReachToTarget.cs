using System;
using Cysharp.Threading.Tasks;
using UnityEngine;

public abstract class ReachToTarget : Interactable
{
    [Space]
    [SerializeField] private Sprite Idle;
    [SerializeField] private Sprite attack;
    public static event Action LossCondition;
    public static event Action WinCondition;
    public EnemyTarget Target { get; set; }
    protected GameData Data;
    protected bool Move = true;
    
    private void Start()
    {
        Idle = Renderer.sprite;
        Data = Resources.Load<GameData>("data");
    }

    private void FixedUpdate()
    {
        if (!Move) return;
        var dir = GetDirection();
        Renderer.flipX = Highlight.flipX = dir.x > 0;
        transform.Translate(dir * (Data.npcSpeed * Time.fixedDeltaTime));

        if (Vector3.Distance(transform.position, Target.Position) < 3f) 
            ReachedTarget();

    }

    private void ReachedTarget()
    {
        Move = false;
        Collider.enabled = false;
        OnReach();
        Target.Interact();
        LossCondition?.Invoke();
        Sound();
    }

    private void OnEnable()
    {
        Renderer.sprite = Idle;
    }

    protected async void OnReach()
    {
        Renderer.sprite = attack;
        await UniTask.Delay(1000);
        Disable();
    }

    protected abstract void Sound();

    protected abstract Vector3 GetDirection();

    protected void InvokeWinCondition() => WinCondition?.Invoke();
}
