using UnityEngine;

public class CurtainOpener : Interactable
{
    [SerializeField] private Animator animator, curtain;
    
    public override void Execute()
    {
        animator.SetTrigger("Fall");
        curtain.SetTrigger("Open");
        Audio.Instance.FadeToGameMusic();
    }

}
