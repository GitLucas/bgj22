using UnityEngine;

public class SpotlightTarget : EnemyTarget
{
    [Space]
    [SerializeField] private Darkness darkness;

    protected override void PreInteraction()
    {
        base.PreInteraction();
        darkness.TryEnable();
    }

    protected override void PostInteraction()
    {
        base.PostInteraction();
        darkness.TryDisable();
    }
}
