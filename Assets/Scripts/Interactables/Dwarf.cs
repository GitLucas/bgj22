using UnityEngine;

public class Dwarf : ReachToTarget
{
    private float _t = 1;

    public override void Execute()
    {
        Collider.enabled = false;
        InvokeWinCondition();
        Audio.Instance.Play(Audio.Library.goblinTapped);
        _t *= -1;
    }

    public override void Enable()
    {
        _t = 1;
        Collider.enabled = true;
        Audio.Instance.Play(Audio.Library.goblinSpawn);
        Move = true;
        base.Enable();
    }
    
    protected override void Sound() => Audio.Instance.Play(Audio.Library.goblinVictory);

    protected override Vector3 GetDirection()
    {
        var position = transform.position;
        var targetDir = (Target.Position - position).normalized;
        var dir = 
            position.y > Data.floorPosition ? 
            Vector3.down + targetDir*.125f : 
            targetDir;

        dir *= _t;
        return dir;
    }
}
