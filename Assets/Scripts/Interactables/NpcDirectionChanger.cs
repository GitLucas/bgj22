using UnityEngine;

[RequireComponent(typeof(WalkingNpc))]
public class NpcDirectionChanger : Interactable
{
    [SerializeField] private WalkingNpc npc;

    private void Reset() => npc = GetComponent<WalkingNpc>();
    
    public override void Execute()
    {
        npc.ToggleDirection();
    }

}
