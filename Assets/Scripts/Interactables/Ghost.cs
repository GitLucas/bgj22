using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;

public class Ghost : ReachToTarget
{
    private float t = 0;

    public override async void Execute()
    {
        Collider.enabled = false;
        Audio.Instance.Play(Audio.Library.ghostTapped);
        await Alpha(1, 0);
        InvokeWinCondition();
        Disable();
    }

    protected override void Sound() => Audio.Instance.Play(Audio.Library.ghostVictory);

    protected override Vector3 GetDirection()
    {
        t += Time.fixedDeltaTime;
        var s = Mathf.Sin(t);
        var dir = (Target.Position - transform.position).normalized;
        dir.x += s;
        return dir;
    }

    public override void Enable()
    {
        Collider.enabled = true;
        gameObject.SetActive(true);
        Audio.Instance.Play(Audio.Library.ghostSpawn);
        Move = true;
        Alpha(0, 1);
    }

    private async Task Alpha(float from, float to)
    {
        var m = from < to ? 1 : -1;
        var a = from;
        var t = Mathf.Abs(to - a);
        var color = Renderer.color;
        
        while (t >= .001f)
        {
            a += m*.1f;
            color.a = a;
            Renderer.color = color;
            t = Mathf.Abs(to - a);
            await UniTask.WaitForFixedUpdate();
        }

        color.a = to;
        Renderer.color = color;
    }
}
