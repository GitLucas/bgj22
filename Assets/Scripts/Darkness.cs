using UnityEngine;

public class Darkness : MonoBehaviour
{
    [SerializeField] private SpriteRenderer Renderer;

    private int _count;
    private readonly object _myLock = new object();

    public void TryEnable()
    {
        lock (_myLock)
        {
            if (_count == 0) Renderer.enabled = true;
            Audio.Instance.Play(Audio.Library.lightsOff);
            _count++;
        }
    }

    public void TryDisable()
    {
        lock (_myLock)
        {
            _count--;
            if (_count == 0) Renderer.enabled = false;
        }
    }

}
