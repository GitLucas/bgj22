using UnityEngine;

public class WalkingNpc : MonoBehaviour
{

    [Range(-1, 1)]
    [SerializeField] private int direction = 1;

    [SerializeField] private SpriteRenderer Renderer;

    private GameData _data;
    private float Speed => _data.npcSpeed;

    public void ToggleDirection()
    {
        direction *= -1;
        Renderer.flipX = !Renderer.flipX;
    }

    private void Awake()
    {
        _data = Resources.Load<GameData>("data");
    }

    private void Update()
    {
        transform.Translate(Vector3.right * (direction * Speed));
    }

    private void Reset()
    {
        Renderer = GetComponent<SpriteRenderer>();
    }
}
