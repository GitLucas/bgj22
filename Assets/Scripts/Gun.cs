﻿using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{

    [SerializeField] private Slider chargeSlider;

    private bool _reloading, _shooting;
    private float _charge = 1f;

    public static event Action OnShoot;
    private GameData _data;

    public float Charge
    {
        get => _charge;
        set
        {
            _charge = value;
            chargeSlider.value = _charge;
        }
    }

    private void Start()
    {
        _data = Resources.Load<GameData>("data");
    }

    public async void Reload()
    {
        if (_reloading) return;
        
        Audio.Instance.Play(Audio.Library.gunReload);
        _reloading = true;
        
        while (Charge < 1f && _reloading)
        {
            Charge += _data.chargePerShot/2f;
            await UniTask.Delay(125);
        }

        _reloading = false;
    }

    public void StopReload()
    {
        _reloading = false;
    }

    public async void Shoot()
    {
        if (Charge < _data.chargePerShot)
        {
            Audio.Instance.Play(Audio.Library.gunFailedShoot);
            return;
        }

        if (_shooting) return;
        _shooting = true;
        
        Audio.Instance.Play(Audio.Library.gunShoot);
        await UniTask.Delay(50);
        OnShoot?.Invoke();
        Charge -= _data.chargePerShot;
        _shooting = false;
    }
    

}