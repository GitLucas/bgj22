using System;
using UnityEngine;

public class Cursor : MonoBehaviour
{
    [SerializeField] private Texture2D empty;
    private Camera _cam;

    private void Start()
    {
        _cam = Camera.main;
        UnityEngine.Cursor.SetCursor(empty, new Vector2(20, 20), CursorMode.Auto);
    }

    private void Update()
    {
        transform.position = _cam.ScreenToWorldPoint(Input.mousePosition + Vector3.forward);
    }
}
